#ifndef TESTING_H
#define TESTING_H

#include <iostream>

#include "queue.h"
#include "linkedList.h"
#include "stack.h"
#include "utils.h"


void test_linked_list();
void test_queue();
void test_stack();
void test_utils();
void test_reverse10();


#endif // TESTING_H