#include "stack.h"
#include "linkedList.h"


void push(stack* s, unsigned int element)
{
	s->head_of_stack = push_befor_head(s->head_of_stack, element);
	s->size++;
}


int pop(stack* s)
{
	if (s->size == 0) { return -1; }

	int res;

	s->head_of_stack = remove_head(s->head_of_stack, &res);
	s->size--;

	return res;
}

void initStack(stack* s)
{
	s->size = 0;
	s->head_of_stack = new node;
	init_list(s->head_of_stack, 0);
}


void cleanStack(stack* s)
{
	s->head_of_stack = delete_list(s->head_of_stack);
	delete s->head_of_stack;
	s->size = 0;
}
