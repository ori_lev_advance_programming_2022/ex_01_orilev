#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <iostream>


typedef struct node
{
	unsigned int val;
	struct node* next;
} node;


// function init the head of the list 
void init_list(node* n, int val);

// function push value befor the head of the list
// return the current head.
node* push_befor_head(node* head, unsigned int value);


// function remove the head and return the value of it.
// put the answer in the res ptr
node* remove_head(node* head, int* res);

// function delete the linked list
node* delete_list(node* head);

#endif // LINKEDKIST_H