#include "queue.h"


void initQueue(queue* q, unsigned int size)
{
    q->is_empty = true;
    q->max_size = size;
    q->current_size = 0;
    q->values = new unsigned int[q->max_size];
}


void cleanQueue(queue* q)
{
    delete[] q->values;
}


bool isEmpty(queue* q) 
{ 
    return q->is_empty;
}


bool isFull(queue* q)
{
    return (q->max_size == q->current_size);
}



void enqueue(queue* q, unsigned int new_data)
{
    if (q->current_size < q->max_size)
    {
        // current size is also a pointer to the current index of the array
        q->values[q->current_size] = new_data;
        q->current_size++;
        // when you push element the queue must be not empty
        q->is_empty = false;
    }
}


int dequeue(queue* q)
{
    unsigned int i = 0;


    // check if queue is empty
    if (q->current_size == 0) { return -1; }

    // get the first element
    unsigned int val = q->values[0];

    // move all the elements to the end of the queue
    for (i = 1; i < q->current_size; i++)
    {
        q->values[i - 1] = q->values[i];
    }

    // delete the last element (because he moved) (prevante duplicate values)
    //q->values[i] = 0;


    // update queue data
    q->current_size--;
    q->is_empty = (q->current_size == 0);

    return val;
}
