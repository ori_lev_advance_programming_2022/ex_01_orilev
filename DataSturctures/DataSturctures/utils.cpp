#include "utils.h"
#include "stack.h"

void reverse(int* nums, unsigned int size)
{
	int i = 0;
	stack* s = new stack;

	initStack(s);


	for (i = 0; i < size; i++)
	{
		push(s, nums[i]);
	}

	for (i = 0; i < size; i++)
	{
		nums[i] = pop(s);
	}

	cleanStack(s);
	delete s;
}


int* reverse10()
{
	int i = 0;
	int j = 0;
	int* arr = new int[10];


	for (i = 0; i < 10; i++)
	{
		std::cout << "Enter number: ";
		std::cin >> j;
		arr[i] = j;
	}

	reverse(arr, 10);

	return arr;
}
