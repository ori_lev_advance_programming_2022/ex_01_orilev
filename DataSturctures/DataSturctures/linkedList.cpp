#include "linkedList.h"

void init_list(node* n, int val)
{
	n->val = val;
	n->next = NULL;
}

// function push value befor the head of the list
node* push_befor_head(node* head, unsigned int value)
{
	node* temp = new node;
	temp->val = value;
	temp->next = head;

	return temp;
}

// function remove the head and return the value of it.
// put the answer in the res ptr
// return the current head.
node* remove_head(node* head, int* res)
{
	node* head_two = NULL;
	if (head == NULL) 
	{
		*res = -1;
		return head;
	}


	*res = head->val;
	head_two = head;
	head = head->next;

	delete head_two;
	return head;
}
// function delete the linked list
node* delete_list(node* head)
{
	node* temp = NULL;

	while (head != NULL)
	{
		temp = head->next;
		delete head;
		head = temp;
	}
	return head;
}
