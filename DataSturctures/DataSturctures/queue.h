#ifndef QUEUE_H
#define QUEUE_H

#include <iostream>


/* a queue contains positive integer values. */
typedef struct queue
{
    unsigned int max_size; //save the maximum size of the queue
    unsigned int current_size; // save how many items are currently in the queue
    bool is_empty; // whether the queue is empty its true
    unsigned int* values; // pointer to int array of values
} queue;

/*
function initial the queue
input: a new queue pointer
output: none
*/
void initQueue(queue* q, unsigned int size);


/*
function clean the queue array
input: Queue to clean
output: none
*/
void cleanQueue(queue* q);



/*
function check if the queue is empty
input: queue to check if it is empty
output: empty -> true, not empty -> false
*/
bool isEmpty(queue* q);




/*
function check if the queue is full
input: queue to check if it is full
output: full -> true, not full -> false
*/
bool isFull(queue* q);



/*
function push new element to the queue
input: queue -> push element into it, new_data -> the element to push
output: none
*/
void enqueue(queue* q, unsigned int new_data);



/*
function return the first element of the queue (or -1 if it is empty)
input: queue to pop from
output: the popped value from the queue (or -1 if it is empty)
*/
int dequeue(queue* q);

#endif /* QUEUE_H */