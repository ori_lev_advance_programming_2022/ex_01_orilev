#include "testing.h"

// test the pueue
void test_queue()
{
    unsigned int temp = 0;
    unsigned int q_size = 0;

    queue* q = new queue;

    std::cout << "Enter the size of the queue: ";
    std::cin >> q_size;

    initQueue(q, q_size);


    std::cout << "\nEnter values to the queue" << std::endl;

    while (!isFull(q))
    {
        std::cout << "Enter number: ";
        std::cin >> temp;
        getchar();
        enqueue(q, temp);
    }

    std::cout << "\n\nQueue Values:" << std::endl;

    while (!isEmpty(q))
    {
        std::cout << dequeue(q) << std::endl;
    }

    cleanQueue(q);

    delete q;
}

// linked list test
void test_linked_list()
{
    node* head = NULL;
    node* copy_head = NULL;
    int i = 0;
    int temp;

    std::cout << "(push the nodes to the head)" << std::endl;


    for (i = 0; i < 4; i++)
    {
        std::cout << "Enter value to the node: ";
        std::cin >> temp;
        head = push_befor_head(head, temp);
    }

    std::cout << "Print the list (pushed from the head, so it be reversed..)" << std::endl;

    copy_head = head;
    while (copy_head != NULL)
    {
        std::cout << copy_head->val << std::endl;
        copy_head = copy_head->next;
    }

    head = remove_head(head, &i);

    std::cout << "should print the first value of the list: " << i << std::endl;

    head = delete_list(head);
}


void test_stack()
{
    stack* s = new stack;

    initStack(s);

    std::cout << "push to the stack 1, 2, 3" << std::endl;

    push(s, 1);
    push(s, 2);
    push(s, 3);

    std::cout << "pop the stack 4 times, expected output: 3, 2, 1, -1" << std::endl;

    std::cout << pop(s) << std::endl;
    std::cout << pop(s) << std::endl;
    std::cout << pop(s) << std::endl;
    std::cout << pop(s) << std::endl;

    std::cout << "push to the stack again 1, 2, 3" << std::endl;
    push(s, 1);
    push(s, 2);
    push(s, 3);

    std::cout << "poped the stack twich, expected output: 3, 2" << std::endl;

    std::cout << pop(s) << std::endl;
    std::cout << pop(s) << std::endl;

    cleanStack(s);
}


void test_utils()
{
    int* a = new int[]{ 1, 2, 3, 4, 5 };
    int i = 0;

    std::cout << "The array: " << std::endl;


    for (i = 0; i < 5; i++)
    {
        std::cout << a[i] << std::endl;
    }

    std::cout << "After reverse: " << std::endl;
    reverse(a, 5);

    for (i = 0; i < 5; i++)
    {
        std::cout << a[i] << std::endl;
    }

    delete[] a;
}


void test_reverse10()
{
    int i = 0;
    int* my_arr = reverse10();

    std::cout << "Sholud print the reversed array:" << std::endl;
    for (i = 0; i < 10; i++)
    {
        std::cout << my_arr[i] << std::endl;
    }

    delete[] my_arr;
}